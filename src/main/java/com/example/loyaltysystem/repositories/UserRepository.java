package com.example.loyaltysystem.repositories;

import com.example.loyaltysystem.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

//    boolean updateProfile(User id);

}

