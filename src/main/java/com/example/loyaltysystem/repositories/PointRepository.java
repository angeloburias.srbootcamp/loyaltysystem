package com.example.loyaltysystem.repositories;

import com.example.loyaltysystem.models.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PointRepository extends JpaRepository<Point, Long> {
    @Query(value = "SELECT COALESCE(SUM(tbl_points.user_balance),0) as balance"+
            "\n"+" FROM tbl_points WHERE tbl_points.user_id = ?1",
            nativeQuery = true)
    int checkPoints(Long id);

    @Query(value = "SELECT * FROM tbl_points WHERE tbl_points.user_id = ?1", nativeQuery = true)
    Optional<Point> findPointByUser(Long id);

}
