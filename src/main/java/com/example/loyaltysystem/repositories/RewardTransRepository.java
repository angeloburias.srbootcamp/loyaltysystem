package com.example.loyaltysystem.repositories;

import com.example.loyaltysystem.models.RewardsTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RewardTransRepository extends JpaRepository<RewardsTransaction, Long> {

    @Query(value = "SELECT * FROM tbl_rewards_trans WHERE tbl_rewards_trans.user_id = ?1", nativeQuery = true)
    List<?> getUserRewardsTrans(Long id);
}
