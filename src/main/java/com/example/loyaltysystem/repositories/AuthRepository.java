package com.example.loyaltysystem.repositories;

import com.example.loyaltysystem.models.Authentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface AuthRepository extends JpaRepository<Authentication, Long> {
    @Query(value = "SELECT COUNT(tbl_auth.username) FROM tbl_auth WHERE tbl_auth.username =?1 AND tbl_auth.password =?2", nativeQuery = true)
    Optional<?> login(String username, String password);

    @Query(value = "SELECT tbl_auth.* FROM tbl_auth WHERE tbl_auth.user_id =?1", nativeQuery = true)
    Optional<Authentication> getUserID(Long user_id);
}
