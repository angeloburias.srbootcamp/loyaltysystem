package com.example.loyaltysystem.repositories;

import com.example.loyaltysystem.models.RewardProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RewardProdRepository extends JpaRepository<RewardProduct, Long> {
    @Query(value = "\n" +
            "SELECT  COALESCE(SUM(tbl_rewards_product.points),0) as rewards "+"\n"+
            "FROM tbl_rewards_product WHERE tbl_rewards_product.id = ?1",
                nativeQuery = true)
    int checkPointsOfReward(Long id);
}
