package com.example.loyaltysystem.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_rewardsProduct")
public class RewardProduct {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "rewardsName")
    private String rewardsName;

    @Column(name = "points")
    private int points;


    @Column(name = "dateCreated")
    private String dateCreated;

    @Column(name = "dateUpdated")
    private String dateUpdated;

    @Column(name = "dateDelete")
    private String dateDelete;

//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "user_id", nullable = false)
//    private User user;
}
