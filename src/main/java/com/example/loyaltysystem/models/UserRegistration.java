package com.example.loyaltysystem.models;

import lombok.Data;

@Data
public class UserRegistration {
    private User user;
    private Authentication auth;

    public UserRegistration(User us, Authentication aut){
        this.user = us;
        this.auth = aut;
    }
}
