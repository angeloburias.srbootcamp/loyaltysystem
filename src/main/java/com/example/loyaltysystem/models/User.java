package com.example.loyaltysystem.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;


@JsonIgnoreProperties({"ignore"})
@Data
@Entity
@Table(name = "tbl_users")
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name= "lastname")
    private String lastname;

    @Column(name = "email")
    private String email;

    @Column(name = "PhoneNumber")
    private String contactNumber;

    @Column(name = "Address")
    private String address;

    @Column(name = "dateCreated")
    private String dateCreated;

    @Column(name = "dateUpdated")
    private String dateUpdated;

    @Column(name = "dateDelete")
    private String dateDelete;

}
