package com.example.loyaltysystem.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_rewardsTrans")
public class RewardsTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private Long user_id;

    @Column(name = "item_id")
    private Long item_id;


    @Column(name = "dateCreated")
    private String dateCreated;

    @Column(name = "dateUpdated")
    private String dateUpdated;

    @Column(name = "dateDelete")
    private String dateDelete;

}
