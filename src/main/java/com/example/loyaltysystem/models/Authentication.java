package com.example.loyaltysystem.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;


@JsonIgnoreProperties({"userprop"})
@Data
@Entity
@Table(name = "tbl_auth")
public class Authentication {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "user_ID")
    private Long user_ID;


    @Column(name = "dateCreated")
    private String dateCreated;

    @Column(name = "dateUpdated")
    private String dateUpdated;

    @Column(name = "dateDelete")
    private String dateDelete;

}
