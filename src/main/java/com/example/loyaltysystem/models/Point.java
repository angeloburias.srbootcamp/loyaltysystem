package com.example.loyaltysystem.models;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_points")
public class Point {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "userBalance")
    private Long userBalance;

    @Column(name ="user_id")
    private Long user_id;


    @Column(name = "dateCreated", nullable = false)
    private String dateCreated;

    @Column(name = "dateUpdated", nullable = false)
    private String dateUpdated;

    @Column(name = "dateDelete", nullable = false)
    private String dateDelete;
//    @OneToOne(fetch = FetchType.LAZY, optional = false)
//    @JoinColumn(name = "user_id", nullable = false)
//    private User user;
}
