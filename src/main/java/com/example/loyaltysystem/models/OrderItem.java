package com.example.loyaltysystem.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@JsonIgnoreProperties({"ignore"})
@Data
@Entity
@Table(name = "tbl_orderItems")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "itemName")
    private String itemName;

    @Column(name = "points")
    private float points;


    @Column(name = "dateCreated")
    private String dateCreated;

    @Column(name = "dateUpdated")
    private String dateUpdated;

    @Column(name = "dateDelete")
    private String dateDelete;

//    @Column(name = "rewardsProductID", nullable = true)
//    private Long productrewardsID;

//    @JsonProperty("ignore")
//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
//    @JoinTable(name = "tbl_rewardsTransactions", joinColumns = { @JoinColumn(name = "rewards_product_ID")}, inverseJoinColumns = {
//            @JoinColumn(name = "user_ID")})
//    private Set<RewardProduct> rewardsProduct = new HashSet<RewardProduct>();

}

