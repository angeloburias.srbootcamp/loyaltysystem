package com.example.loyaltysystem.services;

import com.example.loyaltysystem.models.RewardProduct;
import com.example.loyaltysystem.models.RewardsTransaction;

import java.util.List;
import java.util.Optional;

public interface ILoyaltyServices {
    RewardProduct updateReward(Long id, RewardProduct rp);
    RewardProduct addRewardProduct(RewardProduct rp);
    boolean removeReward(Long id);

    Optional<?> redeemReward(RewardsTransaction rt);

    List<RewardProduct> getRewards();
}
