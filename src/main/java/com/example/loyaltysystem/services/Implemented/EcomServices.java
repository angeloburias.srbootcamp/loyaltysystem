package com.example.loyaltysystem.services.Implemented;

import com.example.loyaltysystem.models.OrderItem;
import com.example.loyaltysystem.models.Point;
import com.example.loyaltysystem.models.RewardProduct;
import com.example.loyaltysystem.models.RewardsTransaction;
import com.example.loyaltysystem.repositories.OrderItemRepository;
import com.example.loyaltysystem.repositories.PointRepository;
import com.example.loyaltysystem.repositories.RewardProdRepository;
import com.example.loyaltysystem.repositories.RewardTransRepository;
import com.example.loyaltysystem.services.IEcommServices;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class EcomServices implements IEcommServices {

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private PointRepository pointRepository;

    @Autowired
    private RewardTransRepository rewardTransRepository;
    @Autowired
    private RewardProdRepository rewardProdRepository;
    @Override
    public Optional<?> createOrder(RewardsTransaction rt) {
        try {
            int currentPts = pointRepository.checkPoints(rt.getUser_id());
            int rewardPts = rewardProdRepository.checkPointsOfReward(rt.getItem_id());
            if(currentPts >= 0){
                    RewardsTransaction rewardsTransaction = new RewardsTransaction();
                    rewardsTransaction.setItem_id(rt.getItem_id());
                    rewardsTransaction.setUser_id(rt.getUser_id());
                    rewardTransRepository.save(rewardsTransaction);

                    int current_balance = currentPts + rewardPts;
                    Optional<Point> getPts = pointRepository.findPointByUser(rt.getUser_id());
                    if(getPts.isPresent()){
                        Point current_pts = getPts.get();
                        current_pts.setUser_id(rt.getUser_id());
                        current_pts.setUserBalance((long)current_balance);
                        pointRepository.save(current_pts);
                    }
                    return Optional.of("Reward points added");
            }else {
                return Optional.of("Insufficient points found");
            }
          }catch (Exception e){
              e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public OrderItem updateOrder(Long id, OrderItem od) {
        Optional<OrderItem> findOrder = orderItemRepository.findById(id);
        if(findOrder.isPresent()){
            OrderItem foundOrder = findOrder.get();
            foundOrder.setItemName(od.getItemName());
            foundOrder.setPoints(od.getPoints());
            return orderItemRepository.save(foundOrder);
        } else return null;
    }

    @Override
    public boolean removeOrder(Long id) {
        Optional<OrderItem> findOrder = orderItemRepository.findById(id);
        if (findOrder.isPresent()){
            orderItemRepository.deleteById(id);
            return true;
        }else return false;
    }

    @Override
    public Optional<List<OrderItem>> getOrders() {
     Optional<List<OrderItem>> list = Optional.of(orderItemRepository.findAll());
     return list.isPresent() ? list :  Optional.empty();
    }


}
