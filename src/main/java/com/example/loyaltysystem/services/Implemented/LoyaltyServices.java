package com.example.loyaltysystem.services.Implemented;

import com.example.loyaltysystem.models.Point;
import com.example.loyaltysystem.models.RewardProduct;
import com.example.loyaltysystem.models.RewardsTransaction;
import com.example.loyaltysystem.repositories.PointRepository;
import com.example.loyaltysystem.repositories.RewardProdRepository;
import com.example.loyaltysystem.repositories.UserRepository;
import com.example.loyaltysystem.services.ILoyaltyServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class LoyaltyServices implements ILoyaltyServices {

    @Autowired
    private RewardProdRepository rewardProdRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PointRepository pointRepository;

    @Override
    public Optional<?> redeemReward(RewardsTransaction rt) {
        Optional<?> currentUser = userRepository.findById(rt.getUser_id());
        if(currentUser.isPresent()) {
            int currentBalance = pointRepository.checkPoints(rt.getUser_id()); // get the sum of user balance
            int requiredPts = rewardProdRepository.checkPointsOfReward(rt.getItem_id());
            if(currentBalance > 0 && currentBalance > requiredPts) { // check if user has enough points
                int final_balance = currentBalance - requiredPts;

                Optional<Point> getUserPts = pointRepository.findPointByUser(rt.getUser_id()); // get user points
                if(getUserPts.isPresent()){
                    Point updatedBalance = getUserPts.get();
                    updatedBalance.setUser_id(rt.getUser_id());
                    updatedBalance.setUserBalance((long) final_balance);
                    pointRepository.save(updatedBalance);
                }
            return Optional.of("Redeem reward successfully");
            }else{
                return Optional.of("Insufficient point found: " + currentBalance);
            }
        }
        return Optional.empty();
    }

    @Override
    public List<RewardProduct> getRewards() {
        return rewardProdRepository.findAll();
    }

    @Override
    public RewardProduct updateReward(Long id, RewardProduct rp) {
        Optional<RewardProduct> findprod = rewardProdRepository.findById(id);
        if(findprod.isPresent()){
            RewardProduct product = findprod.get();
            product.setRewardsName(rp.getRewardsName());
            product.setPoints(rp.getPoints());
            return rewardProdRepository.save(product);
        }else return null;
    }

    @Override
    public RewardProduct addRewardProduct(RewardProduct rp) {
        return rewardProdRepository.save(rp);
    }

    @Override
    public boolean removeReward(Long id) {
       Optional<RewardProduct> rp = rewardProdRepository.findById(id);
        if (rp.isPresent()){
            rewardProdRepository.deleteById(id);
            return true;
        } else return false;
    }


}
