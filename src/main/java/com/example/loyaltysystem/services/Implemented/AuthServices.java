package com.example.loyaltysystem.services.Implemented;

import com.example.loyaltysystem.models.Authentication;
import com.example.loyaltysystem.repositories.AuthRepository;
import com.example.loyaltysystem.repositories.UserRepository;
import com.example.loyaltysystem.services.IAuthServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthServices implements IAuthServices {

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private UserRepository userRepository;
    @Override
    public boolean Login(Authentication auth) {
        if(auth.getPassword().isBlank() && auth.getPassword().isBlank()) {
            return false;
        }
        else {
           Optional<?> result = authRepository.login(auth.getUsername(), auth.getPassword());
           return true;
        }
    }

    @Override
    public void Logout() {
    }

    @Override
    public boolean updatePassword(Long id, Authentication auth) {
        Optional<Authentication> currentAuth = authRepository.getUserID(id);
        if(!currentAuth.isEmpty()){
            Authentication aut = currentAuth.get();
            aut.setUsername(auth.getUsername());
            aut.setPassword(auth.getPassword());
            authRepository.save(aut);
            return true;
        }
        return false;
    }

    @Override
    public Authentication addAuth(Authentication auth) {
        try{
            Authentication saveAuth = authRepository.save(auth);
            return saveAuth;
        }catch (Exception e){
            e.printStackTrace();
        }
      return null;
    }

}
