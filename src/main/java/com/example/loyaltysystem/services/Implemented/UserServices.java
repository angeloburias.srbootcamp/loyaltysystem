package com.example.loyaltysystem.services.Implemented;



import com.example.loyaltysystem.models.User;
import com.example.loyaltysystem.repositories.UserRepository;
import com.example.loyaltysystem.services.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServices implements IUserServices {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<?> userProfile(Long id) {
        Optional<?> findprofile = userRepository.findById(id);
        if(findprofile.isPresent()){
            return userRepository.findById(id);
        }
        return Optional.empty();
    }
    @Override
    public User addUser(User us) {
        try{
            return userRepository.save(us);
        }catch (Exception e){
            e.printStackTrace();
        }
       return null;
    }

    @Override
    public boolean updateProfile(Long id, User user) {
        Optional<User> us = userRepository.findById(id);

        if(us.isPresent()){
            var result = us.get();
            result.setFirstname(user.getFirstname());
            result.setLastname(user.getLastname());
            result.setEmail(user.getEmail());
            result.setAddress(user.getAddress());
            result.setContactNumber(user.getContactNumber());

            userRepository.save(result);
            return true;
        }

        return false;
    }

    @Override
    public List<?> getUsers() {
        return userRepository.findAll();
    }
}
