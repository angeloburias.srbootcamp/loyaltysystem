package com.example.loyaltysystem.services.Implemented;

import com.example.loyaltysystem.models.RewardsTransaction;
import com.example.loyaltysystem.repositories.RewardTransRepository;
import com.example.loyaltysystem.services.IRewardTransServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RewardTransServices implements IRewardTransServices {

    @Autowired
    private RewardTransRepository rewardTransRepository;

    @Override
    public boolean addRewardTrans(RewardsTransaction rewardsTransaction) {
        rewardTransRepository.save(rewardsTransaction);
        return true;
    }

    @Override
    public List<?> getUserRewards(Long id) {
        return rewardTransRepository.getUserRewardsTrans(id);
    }
}
