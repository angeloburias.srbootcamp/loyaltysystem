package com.example.loyaltysystem.services;

import com.example.loyaltysystem.models.User;

import java.util.List;
import java.util.Optional;

public interface IUserServices {
    Optional<?> userProfile(Long id);
    User addUser(User user);
    boolean updateProfile(Long id,User user);
    List<?> getUsers();

}
