package com.example.loyaltysystem.services;

import com.example.loyaltysystem.models.RewardsTransaction;
import com.example.loyaltysystem.services.Implemented.RewardTransServices;

import java.util.List;

public interface IRewardTransServices {
    boolean addRewardTrans(RewardsTransaction rewardsTransaction);

    List<?> getUserRewards(Long id);
}
