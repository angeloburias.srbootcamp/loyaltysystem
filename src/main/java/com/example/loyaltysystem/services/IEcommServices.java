package com.example.loyaltysystem.services;

import com.example.loyaltysystem.models.OrderItem;
import com.example.loyaltysystem.models.RewardProduct;
import com.example.loyaltysystem.models.RewardsTransaction;

import java.util.List;
import java.util.Optional;

public interface IEcommServices {

    Optional<?> createOrder(RewardsTransaction rt);

    OrderItem updateOrder(Long id, OrderItem od);

    boolean removeOrder(Long id);

    Optional<List<OrderItem>> getOrders();

}
