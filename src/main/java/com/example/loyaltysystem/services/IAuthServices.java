package com.example.loyaltysystem.services;

import com.example.loyaltysystem.models.Authentication;

public interface IAuthServices {

    boolean Login(Authentication auth);

    void Logout();

    boolean updatePassword(Long id, Authentication auth);


    Authentication addAuth(Authentication auth);
}
