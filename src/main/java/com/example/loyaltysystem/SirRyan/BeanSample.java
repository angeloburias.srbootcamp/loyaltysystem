package com.example.loyaltysystem.SirRyan;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanSample{
    @Bean
    public String getHello(){
        return "Hello";
    }
}