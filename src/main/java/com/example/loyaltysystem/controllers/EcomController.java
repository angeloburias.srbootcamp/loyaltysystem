package com.example.loyaltysystem.controllers;

import com.example.loyaltysystem.models.OrderItem;
import com.example.loyaltysystem.models.RewardsTransaction;
import com.example.loyaltysystem.services.IEcommServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class EcomController {

    public EcomController(){ }
    @Autowired
    private IEcommServices iEcommServices;

    @PostMapping("/createOrder")
    public ResponseEntity<?> createOrder(@RequestBody RewardsTransaction rt){
        return new ResponseEntity<>(iEcommServices.createOrder(rt), HttpStatus.OK);
    }

    @PutMapping("/updateOrder/{id}")
    public ResponseEntity<?> updateOrder(@PathVariable("id")Long id, @RequestBody OrderItem orderItem){
        return new ResponseEntity<>(iEcommServices.updateOrder(id, orderItem), HttpStatus.OK);
    }

    @GetMapping("/getOrders/")
    public ResponseEntity<?> getOrders(){
        return new ResponseEntity<>(iEcommServices.getOrders(), HttpStatus.OK);
    }

    @DeleteMapping("/deleteOrder/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable("id")Long id){
        return new ResponseEntity<>(iEcommServices.removeOrder(id), HttpStatus.OK);
    }

}
