package com.example.loyaltysystem.controllers;


import com.example.loyaltysystem.models.RewardProduct;
import com.example.loyaltysystem.models.RewardsTransaction;
import com.example.loyaltysystem.services.ILoyaltyServices;
import com.example.loyaltysystem.services.IRewardTransServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class RewardsRedemptionControllers {

    public RewardsRedemptionControllers(){}
    @Autowired
    private ILoyaltyServices iLoyaltyServices;
    @Autowired
    private IRewardTransServices iRewardTransServices;
    @PostMapping("/redeemReward")
    public ResponseEntity<?> redeemReward( @RequestBody RewardsTransaction rt){
        return new ResponseEntity<>(iLoyaltyServices.redeemReward(rt), HttpStatus.OK);
    }

    @PostMapping("/addReward/")
    public ResponseEntity<?> addReward(@RequestBody RewardProduct rp){
        return new ResponseEntity<>(iLoyaltyServices.addRewardProduct(rp), HttpStatus.OK);
    }

    @PutMapping("/updateReward/{id}")
    public ResponseEntity<?> updateReward(@PathVariable("id") Long id, @RequestBody RewardProduct rp){
        return new ResponseEntity<>(iLoyaltyServices.updateReward(id, rp), HttpStatus.OK);
    }

    @GetMapping("/getRewards/")
    public ResponseEntity<?> getRewards(){
        return new ResponseEntity<>(iLoyaltyServices.getRewards(), HttpStatus.OK);
    }

    @GetMapping("/getUserRewards/{id}")
    public ResponseEntity<?> getUserRewardsTransactions(@PathVariable("id") Long id){
        return new ResponseEntity<>(iRewardTransServices.getUserRewards(id), HttpStatus.OK);
    }


    @DeleteMapping("/removeReward/{id}")
    public ResponseEntity<?> removeReward(@PathVariable("id") Long id){
        if (iLoyaltyServices.removeReward(id)) {
            return new ResponseEntity<>("remove successfully ID:" + id, HttpStatus.OK);
        }else return new ResponseEntity<>("failed to remove", HttpStatus.OK);
    }

}
