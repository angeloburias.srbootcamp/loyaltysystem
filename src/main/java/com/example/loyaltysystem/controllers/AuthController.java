package com.example.loyaltysystem.controllers;


import com.example.loyaltysystem.models.Authentication;

import com.example.loyaltysystem.services.IAuthServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class AuthController {
    @Autowired
    private IAuthServices IauthServices;

    public AuthController(){}

    @PutMapping("/login")
    public ResponseEntity<?> login(@RequestBody Authentication auth) {
        return new ResponseEntity<>(IauthServices.Login(auth), HttpStatus.CREATED);
    }

    @PutMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestBody Authentication auth) {
        return new ResponseEntity<>(IauthServices.updatePassword(auth.getUser_ID(), auth), HttpStatus.CREATED);
    }
}
