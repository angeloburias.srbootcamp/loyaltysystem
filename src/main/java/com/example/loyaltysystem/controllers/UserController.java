package com.example.loyaltysystem.controllers;

import com.example.loyaltysystem.models.Authentication;
import com.example.loyaltysystem.models.User;
import com.example.loyaltysystem.models.UserRegistration;
import com.example.loyaltysystem.services.IAuthServices;
import com.example.loyaltysystem.services.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {

    public UserController(){}

    @Autowired
    private IUserServices IuserServices;

    @Autowired
    public IAuthServices IauthServices;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserRegistration userR) {
        Authentication aut = userR.getAuth();
        var us = IuserServices.addUser(userR.getUser());
        aut.setUser_ID(us.getId());
        IauthServices.addAuth(aut);
        return new ResponseEntity<>(userR, HttpStatus.CREATED);
    }

    @GetMapping("/getProfile/{id}")
    public ResponseEntity<?> getProfile(@PathVariable("id") Long id){
        return new ResponseEntity<>(IuserServices.userProfile(id), HttpStatus.OK);
    }

    @GetMapping("/getUsers/")
    public ResponseEntity<?> getUsers(){
        return new ResponseEntity<>(IuserServices.getUsers(), HttpStatus.OK);
    }

    @PutMapping("/updateProfile/{id}")
    public ResponseEntity<?> updateProfile(@PathVariable("id") Long id, @RequestBody User user){
        return new ResponseEntity<>(IuserServices.updateProfile(id, user), HttpStatus.OK);
    }
}
